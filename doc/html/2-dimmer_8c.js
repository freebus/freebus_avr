var 2_dimmer_8c =
[
    [ "MAXZEIT", "2-dimmer_8c.html#aeb27128fe3c76cf8a669fbb420c14454", null ],
    [ "ISR", "2-dimmer_8c.html#ab70da2aaa3ee7e1b1a9c37f48d85d660", null ],
    [ "ISR", "2-dimmer_8c.html#a22acfb428840c6d9aa212764589cf6c6", null ],
    [ "main", "2-dimmer_8c.html#a840291bc02cba5474a4cb46a9b9566fe", null ],
    [ "nulldurchgang", "2-dimmer_8c.html#a71a75372a794add66e109d83d9432f86", null ],
    [ "TWIS_ReadAck", "2-dimmer_8c.html#a69ce247d23ca6be7f8de52cf65672c67", null ],
    [ "TWIS_ReadNack", "2-dimmer_8c.html#a52e35c7d668fa2cc29fc2c8c0bd65edc", null ],
    [ "TWIS_ResonseRequired", "2-dimmer_8c.html#a00acd57e50b1f259871e44010732081c", null ],
    [ "TWIS_Stop", "2-dimmer_8c.html#a4ccb93c7481cdfb82cd5cf33133149dc", null ],
    [ "USART_Init", "2-dimmer_8c.html#aba398ef6d2b9f80899b2d65ca9a0fe7d", null ],
    [ "K1dimmwert_ausgang", "2-dimmer_8c.html#afa2418040358b02bc704c90191a38b2c", null ],
    [ "K2dimmwert_ausgang", "2-dimmer_8c.html#a8604fce8becd3b1eea9166e1db79c73e", null ],
    [ "teiler", "2-dimmer_8c.html#ae7f812e8a82b82d2edbb5d7acb2f63f5", null ],
    [ "zl1", "2-dimmer_8c.html#aeb77eb08acb0c5e22dc0657910b89579", null ],
    [ "zl2", "2-dimmer_8c.html#a5692acbb8dd2ae2403b10b476ec56765", null ],
    [ "zl_50hz", "2-dimmer_8c.html#a42f620b65af79de0a0e51df9b39805b4", null ]
];