var structFBAppInfo =
[
    [ "AppMain", "structFBAppInfo.html#a64ccac9715bda443f0f64a047be8fe93", null ],
    [ "AppReadObject", "structFBAppInfo.html#a4d89002aed02a41ed1f0f566c8dafa65", null ],
    [ "AppSave", "structFBAppInfo.html#a3ebc0e9d0ddce67e8ca37404b5f2c5e5", null ],
    [ "AppUnload", "structFBAppInfo.html#a310be754bcae0e81a16abaacfd88c5c6", null ],
    [ "AppWriteObject", "structFBAppInfo.html#a3ba26890e66553a6c150997970518f6d", null ],
    [ "assoctable", "structFBAppInfo.html#a0edad03fb72637a8ee6e6c1f470248ed", null ],
    [ "comobjtable", "structFBAppInfo.html#ac184969d78f9a8977aa8c758468d6088", null ],
    [ "FBApiVersion", "structFBAppInfo.html#ad098821dd940934ba2e2ae5b2dbf6cf1", null ],
    [ "grpaddr", "structFBAppInfo.html#a3948c95af71587b279401abe979dbb73", null ],
    [ "pParam", "structFBAppInfo.html#a9b71828c095cafac1b56b36eccbaf88d", null ],
    [ "ramflags", "structFBAppInfo.html#ab907ea22f0a5de335242b82bcb7bb844", null ]
];