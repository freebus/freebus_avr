var freebus_avr_8h =
[
    [ "DISABLE_IRQS", "freebus-avr_8h.html#afb4bc69b1f4eb2a776d71f1649bacef3", null ],
    [ "DISABLE_WATCHDOG", "freebus-avr_8h.html#a020f04e5cfb6b8b8a65f9aade81ff567", null ],
    [ "ENABLE_ALL_INTERRUPTS", "freebus-avr_8h.html#a079ee09f95ab03d8a0ca20e54920c03d", null ],
    [ "ENABLE_IRQS", "freebus-avr_8h.html#acf6ee0fe76483795ece779d95cd17b6a", null ],
    [ "ENABLE_WATCHDOG", "freebus-avr_8h.html#a9b64a1a48796b8f2705d35cd6c5a7742", null ],
    [ "IO_GET", "freebus-avr_8h.html#a6e3399f0a0adfaf560a2fb5d34b5446c", null ],
    [ "IO_SET", "freebus-avr_8h.html#a8031a6523bd836ccb71ea60a86578e20", null ],
    [ "IO_SET_DIR", "freebus-avr_8h.html#ac487b376c42bbeab011f5d8815195e1b", null ],
    [ "prog_uint8_t", "freebus-avr_8h.html#a1292f2f164a1997206e9cf714f2372fe", null ]
];