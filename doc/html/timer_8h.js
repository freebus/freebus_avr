var timer_8h =
[
    [ "M2TICS", "timer_8h.html#afd4c8aadeb71325b4ceb275c3435e6af", null ],
    [ "SEC2TICS", "timer_8h.html#aba87006db96c2ae0a880191d26737427", null ],
    [ "timed_out", "timer_8h.html#a08e469e7195e8526f2ccc2c5747fade9", null ],
    [ "TIMER_EXT", "timer_8h.html#ab0e7fc0912f8318392dd5dca71eb88dc", null ],
    [ "timer_t", "timer_8h.html#a3b72c02431c8c631ab35793169a96ca0", null ],
    [ "alloc_timer", "timer_8h.html#a2c83029b25ff5d8e91e8e0b49a3cbd19", null ],
    [ "check_timeout", "timer_8h.html#a4ea465aab85e85e74404d1e2bf8058b5", null ],
    [ "get_ticks", "timer_8h.html#afdbd07452394c966abbd340c165b08a7", null ],
    [ "timer_init", "timer_8h.html#a9c43f6057df1052245ae873a569220ed", null ]
];