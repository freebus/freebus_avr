var NAVTREE =
[
  [ "FreeBusAVR", "index.html", [
    [ "Setting Fuses for ATMega168(P)", "index.html#fuses-168", null ],
    [ "Setting Fuses for ATMega328P(A)", "index.html#fuses-328", null ],
    [ "Compile and create HEX files", "index.html#compile", [
      [ "Windows", "index.html#compile-windows", null ],
      [ "Linux/Unix/MacOSX", "index.html#compile-linux", null ]
    ] ],
    [ "General notes if developing your own application:", "index.html#notes", null ],
    [ "Firmware to link to", "index.html#firmware", [
      [ "Examples", "index.html#example", null ]
    ] ],
    [ "README", "md_README.html", null ],
    [ "Todo List", "todo.html", null ],
    [ "Bug List", "bug.html", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"1wire_8h_source.html",
"fb__in8__app_8c.html#af836f9fbc80bbf6d627c7c12ce391ca7ad8d7de82132817c6bd1afce29d78db20",
"fb__rgb__dim__app_8h.html#a225aa0a0913fcdf60e7e786e4cf5f58c",
"freebus-debug_8h.html#a03ae420828c2fb5e870f8807b4620863"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';