var fb__rgb__dim__app_8h =
[
    [ "APP_BASE_DIMMING_STEP", "fb__rgb__dim__app_8h.html#a6d3f170a509698154149b9d98c6d19e2", null ],
    [ "APP_BRIGHTNESS_BLOCKING_FUNCTION_CH1", "fb__rgb__dim__app_8h.html#af33c053c2bc59703eb8d44fa07ea14da", null ],
    [ "APP_EXT", "fb__rgb__dim__app_8h.html#a3cf2a39427976f0a58a12efa30fc4d98", null ],
    [ "APP_FACTOR_DIMMING_STEP_CH1", "fb__rgb__dim__app_8h.html#abe2ccec7de3e73343250d552807d69b6", null ],
    [ "APP_FACTOR_DIMMING_STEP_CH2", "fb__rgb__dim__app_8h.html#ae36cec3e5120993afdc86ea134236310", null ],
    [ "APP_LOCK_FUNCTION", "fb__rgb__dim__app_8h.html#acd7910518bc8b9ae5b1f4e972ebf5c7b", null ],
    [ "APP_SOFT_OFF_BASE", "fb__rgb__dim__app_8h.html#a9f44426cb7f118293eef1f8cd499d106", null ],
    [ "APP_SOFT_OFF_FACTOR_CH1", "fb__rgb__dim__app_8h.html#ab1b4532f3d7f7e835ee0b19a55d21896", null ],
    [ "APP_SOFT_ON_BASE", "fb__rgb__dim__app_8h.html#abac830d3a7b6b257d4f7299352b5abfe", null ],
    [ "APP_SOFT_ON_FACTOR_CH1", "fb__rgb__dim__app_8h.html#aa6d452a73ecf706cb1df9219320eec70", null ],
    [ "APP_SWITCH_ON_BRIGHTNESS", "fb__rgb__dim__app_8h.html#a2f8210ef678cfad1a56054cc189ef5a8", null ],
    [ "BLUE_VAL", "fb__rgb__dim__app_8h.html#ac36adc7e2ea4be7e85f868e208c1df48", null ],
    [ "GREEN_SAT", "fb__rgb__dim__app_8h.html#a516a3cea32c63f5737064a3bdff4f51e", null ],
    [ "MAX_NUMBER_OF_LEDS", "fb__rgb__dim__app_8h.html#a1763d314007c06b832cb4de49977aaf6", null ],
    [ "OBJECT_BRIGHTNESS_BLUE_VAL", "fb__rgb__dim__app_8h.html#ada2dca74ef75365da42110b05d5c5ef5", null ],
    [ "OBJECT_BRIGHTNESS_GREEN_SAT", "fb__rgb__dim__app_8h.html#a824cf4c0d2eb76b6a59d14d0c579a3a6", null ],
    [ "OBJECT_BRIGHTNESS_RED_HUE", "fb__rgb__dim__app_8h.html#af4a65549a1e6500a6074b98401081a19", null ],
    [ "OBJECT_DIM_BLUE_VAL", "fb__rgb__dim__app_8h.html#a0d46eb2c6297b391a343de0f099cbe4c", null ],
    [ "OBJECT_DIM_GREEN_SAT", "fb__rgb__dim__app_8h.html#ad66382cb8cb09cab0c2fd4d278dba4d0", null ],
    [ "OBJECT_DIM_INDIVIDUALLY", "fb__rgb__dim__app_8h.html#a6e563245177d7b557d1379c637ed5be4", null ],
    [ "OBJECT_DIM_RED_HUE", "fb__rgb__dim__app_8h.html#afba4731b147d1c0ea2b5621ac016901f", null ],
    [ "OBJECT_DIM_RUNS_RESPONSE", "fb__rgb__dim__app_8h.html#a76c5a9eb6460eed062130d302574c10d", null ],
    [ "OBJECT_SWITCH", "fb__rgb__dim__app_8h.html#a92c26dfac8e0d942aa20789c43e924fd", null ],
    [ "OBJECT_SWITCHING_RESPONSE", "fb__rgb__dim__app_8h.html#acb1e1f444368231644294bda9d70ae19", null ],
    [ "POWER_ON_DELAY", "fb__rgb__dim__app_8h.html#aa8149c719f0a95f71872fc7159a80b7f", null ],
    [ "PWM_DELAY_TIME", "fb__rgb__dim__app_8h.html#a225aa0a0913fcdf60e7e786e4cf5f58c", null ],
    [ "PWM_SETPOINT", "fb__rgb__dim__app_8h.html#a0c9795a680ad22be84b865d2b1fcec5a", null ],
    [ "RED_HUE", "fb__rgb__dim__app_8h.html#a54bb7d256a4ef7316f25f620a5d00e74", null ],
    [ "RELAY", "fb__rgb__dim__app_8h.html#a54a4c7f76f3b4b1e70bd8862ccf27bcd", null ],
    [ "PROGMEM", "fb__rgb__dim__app_8h.html#a58107f8061e93957a321cd37199d8602", null ]
];